const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.get('/', (req, res) => {
  console.log('original',req.originalUrl)
  console.log('query',req.query)
  console.log('params',req.param)
  res.send('Hello World!')
})

app.post('/webhook', (req, res) => {
    // let reply_token = req.body.events[0].replyToken
    if(req.headers['x-line-signature']){
      console.log('x-signature',req.headers['x-line-signature'])
    }
    
    console.log(req.body)
    let msg = req.body.events[0].message.text
    console.log('event',req.body.events[0])
    let source = req.body.events[0].source
     console.log(source)
    // reply(reply_token, msg)
    res.sendStatus(200)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
